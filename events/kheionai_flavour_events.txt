# kheionai and cleaved sea flavour events
namespace = kheionai_flavour

########################### KHEIONAI FLAVOUR EVENTS ############################

# by takasaki
country_event = { # Hounds of Askovoinos / Kimanis Revolt Event
	id = kheionai_flavour.1
	title = kheionai_flavour.1.t
	desc = kheionai_flavour.1.d
	picture = CONQUEST_eventPicture
	
	is_triggered_only = yes
	fire_only_once = yes
	major = yes # making this a major event so others aren't blindsided.
	major_trigger = {
		culture_group = kheionai_ruinborn_elf
	}
	
	trigger = {
		religion = kheionism
		capital_scope = {
			region = alecand_region
		}
		owns = 2732
		is_year = 1470
	}
	
	immediate = {
		add_stability = -1
		
		hidden_effect = {		
			2732 = { # cedes kimanis prov to kimanis
				cede_province = G61
				add_core = ROOT # for oktikheion if you somehow haven't cored it
			}
			every_owned_province = { # cedes owned kimabhen provinces to kimanis, gives a core on it if they somehow lost it
				limit = {
					culture = kimabhen
					region = alecand_region
				}
				cede_province = G61
				add_core = G61
			}
			
			2732 = { # creates 7 infantry regiments in kimanis to complement the 5 they get.
				infantry = G61
				infantry = G61
				infantry = G61
				infantry = G61
				infantry = G61
				infantry = G61
				infantry = G61
			}
			G61 = {
				setup_vision = yes
			}
		}
	}
	
	option = { # Kimanis revolts
		name = kheionai_flavour.1.a
		ai_chance = { factor = 100 }
		
		declare_war_with_cb = {
			who = G61
			casus_belli = cb_core
			war_goal_province = 2732
		}
		
		custom_tooltip = kf_tt_kimanis_war
		hidden_effect = {
			if = { # if player is a rival of kimanis revolt tag, they can 100% intervene
				limit = {
					any_enemy_country = {
						capital_scope = { superregion = kheionai_superregion }
						ai = no
					}
				}
				random_enemy_country = {
					limit = {
						capital_scope = { superregion = kheionai_superregion }
						ai = no
					}
					country_event = { id = kheionai_flavour.2 } # intervene in kimanis
				}
			}
			
			else = { # no player is a rival of kimanis, so a random rival intervenes
				random_enemy_country = {
					limit = {
						capital_scope = { superregion = kheionai_superregion }
						is_at_war = NO
					}
					country_event = { id = kheionai_flavour.2 } # intervene in kimanis
				}
			}
		}
	}
	
	option = { # play as Kimanis, same as above but you switch at the end
		name = kheionai_flavour.1.b
		ai_chance = { factor = 0 }
		
		custom_tooltip = kf_tt_kimanis_switch
		hidden_effect = {
			declare_war_with_cb = {
				who = G61
				casus_belli = cb_core
				war_goal_province = 2732
			}
			
			if = { # if player is a rival of kimanis revolt tag, they can 100% intervene
				limit = {
					any_enemy_country = {
						capital_scope = { superregion = kheionai_superregion }
						ai = no
					}
				}
				random_enemy_country = {
					limit = {
						capital_scope = { superregion = kheionai_superregion }
						ai = no
					}
					country_event = { id = kheionai_flavour.2 } # intervene in kimanis
				}
			}
			
			else = { # no player is a rival of kimanis, so a random rival intervenes
				random_enemy_country = {
					limit = {
						capital_scope = { superregion = kheionai_superregion }
						is_at_war = NO
					}
					country_event = { id = kheionai_flavour.2 } # intervene in kimanis
				}
			}
		}
		switch_tag = G61 #Kimanis
	}
}

country_event = { # intervene in Kimanis
	id = kheionai_flavour.2
	title = kheionai_flavour.2.t
	desc = kheionai_flavour.2.d
	picture = CONQUEST_eventPicture
	
	is_triggered_only = yes
	
	option = { # join war
		name = kheionai_flavour.2.a
		join_all_defensive_wars_of = G61
		create_alliance = G61
	}
	
	option = { # refuse to join war
		name = kheionai_flavour.2.b
		add_prestige = -10
	}
}
